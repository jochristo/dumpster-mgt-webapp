package org.joch.dump.core.exception;

import org.joch.dump.api.error.ApiErrorCode;

/**
 *
 * @author ic
 */
public class GeneralApplicationCoreException extends AbstractCoreApplicationException
{
    
    public GeneralApplicationCoreException(ApiErrorCode apiErrorCode, String details) {
        super(apiErrorCode, details);
    }
    
}
