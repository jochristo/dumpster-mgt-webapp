package org.joch.dump.core.exception;

import org.joch.dump.api.error.ApiErrorCode;

/**
 *
 * @author ic
 */
public class InvalidArgumentTypeException extends AbstractCoreApplicationException{
    
    public InvalidArgumentTypeException(String details, String title) {
        super(details, title);
    }

    public InvalidArgumentTypeException(ApiErrorCode apiErrorCode, String details) {
        super(apiErrorCode, details);
    }
    
    
    
}
