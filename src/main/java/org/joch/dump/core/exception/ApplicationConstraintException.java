package org.joch.dump.core.exception;

import org.joch.dump.api.error.ApiErrorCode;

/**
 *
 * @author ic
 */
public class ApplicationConstraintException extends AbstractCoreApplicationException
{
    public ApplicationConstraintException(ApiErrorCode apiErrorCode, String details) {
        super(ApiErrorCode.CONSTRAINT_VIOLATION, details);
    }    
    
}
