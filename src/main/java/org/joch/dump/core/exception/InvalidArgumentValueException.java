package org.joch.dump.core.exception;

import org.joch.dump.api.error.ApiErrorCode;

/**
 *
 * @author ic
 */
public class InvalidArgumentValueException extends AbstractCoreApplicationException
{
    
    public InvalidArgumentValueException(String details, String title) {
        super(details, title);
    }

    public InvalidArgumentValueException(ApiErrorCode apiErrorCode, String details) {
        super(apiErrorCode, details);
    }
    
    
    
}
