package org.joch.dump.data.mongodb.listener;

import org.joch.dump.api.error.ApiErrorCode;
import org.joch.dump.api.exception.InvalidParameterArgumentException;
import org.joch.dump.api.domain.Dumpster;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeSaveEvent;

/**
 *
 * @author ic
 */
public class DumpsterInfoMongoEventListener extends AbstractMongoEventListener<Dumpster>
{    
    private static final Logger logger = LoggerFactory.getLogger(DumpsterInfoMongoEventListener.class);      

    @Override
    public void onBeforeSave(BeforeSaveEvent<Dumpster> event)
    {        
        // check business rules, duplicates, etc
        Dumpster dumpsterInfo = event.getSource();                
        if(dumpsterInfo.getAvailable()  != dumpsterInfo.getTotal() - dumpsterInfo.getAllocated() - dumpsterInfo.getReserved())
        {
            logger.info("Invoking dumpster service listener onBeforeService...");
            throw new InvalidParameterArgumentException(ApiErrorCode.INVALID_PARAMETER_VALUE, "Total should be equal to the sum of allocated, available, and reserved");
        }
        
    }
    
}
