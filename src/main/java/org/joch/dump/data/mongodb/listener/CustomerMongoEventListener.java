
package org.joch.dump.data.mongodb.listener;

import org.joch.dump.api.error.ApiErrorCode;
import org.joch.dump.core.exception.DataConstraintViolationException;
import org.joch.dump.api.domain.Customer;
import org.joch.dump.data.mongodb.service.IMongoDbService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.AfterConvertEvent;
import org.springframework.data.mongodb.core.mapping.event.AfterDeleteEvent;
import org.springframework.data.mongodb.core.mapping.event.AfterLoadEvent;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.data.mongodb.core.mapping.event.BeforeDeleteEvent;
import org.springframework.data.mongodb.core.mapping.event.BeforeSaveEvent;
import org.springframework.data.mongodb.core.mapping.event.MongoMappingEvent;

/**
 *
 * @author ic
 */
public class CustomerMongoEventListener extends AbstractMongoEventListener<Customer>
{
    @Autowired
    private IMongoDbService<Customer> service;

    @Override
    public void onBeforeDelete(BeforeDeleteEvent<Customer> event) {
        super.onBeforeDelete(event); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onAfterDelete(AfterDeleteEvent<Customer> event) {
        super.onAfterDelete(event); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onAfterConvert(AfterConvertEvent<Customer> event) {
        super.onAfterConvert(event); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onAfterLoad(AfterLoadEvent<Customer> event) {
        super.onAfterLoad(event); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onBeforeSave(BeforeSaveEvent<Customer> event) {
        // check business rules, duplicates, etc
        String email = event.getSource().getEmail();
        if(service.findOne("email", email) != null) throw new DataConstraintViolationException(ApiErrorCode.DATA_INTEGRITY_VIOLATION, "Customer email exists and it would create duplicate entries");
        
    }

    @Override
    public void onBeforeConvert(BeforeConvertEvent<Customer> event) {
        super.onBeforeConvert(event); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onApplicationEvent(MongoMappingEvent<?> event) {
        super.onApplicationEvent(event); //To change body of generated methods, choose Tools | Templates.
    }
    
}
