package org.joch.dump.data.mongodb;

import com.mongodb.Mongo;
import org.joch.dump.data.mongodb.listener.CustomerMongoEventListener;
import org.joch.dump.data.mongodb.listener.DumpsterInfoMongoEventListener;
import org.joch.dump.data.mongodb.listener.PlacementMongoEventListener;
import org.joch.dump.data.mongodb.listener.ReservationMongoEventListener;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoClientFactoryBean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 *
 * @author ic
 */
@Configuration
@EnableMongoRepositories(basePackages = "org.joch.dump.api.repository")
public class MongoDbConfiguration
{
    @Value("${spring.data.mongodb.database}")
    private String databaseName;    
        
    @Bean
    public MongoTemplate mongoTemplate(Mongo mongo) throws Exception {                
        return new MongoTemplate(mongoFactoryBean().getObject(), databaseName);
    }    
    
    @Bean
    public MongoClientFactoryBean mongoFactoryBean() {        
    return new MongoClientFactoryBean();
    }            
      
    /*
    @Bean
    public PlacementService placementService(MongoTemplate mongo) {
       return new PlacementService();
    } 
    
    @Bean
    public ReservationService reservationService(MongoTemplate mongo) {
       return new ReservationService();
    }     
    */
    
    // Register domain listeners
    
    @Bean
    public CustomerMongoEventListener customerMongoEventListener(){
        return new CustomerMongoEventListener();
    }
    
    @Bean
    public ReservationMongoEventListener reservationMongoEventListener(){
        return new ReservationMongoEventListener();
    } 
    
    @Bean
    public PlacementMongoEventListener placementMongoEventListener(){
        return new PlacementMongoEventListener();
    }
    
    @Bean
    public DumpsterInfoMongoEventListener dumpsterInfoMongoEventListener(){
        return new DumpsterInfoMongoEventListener();
    }
    
}
