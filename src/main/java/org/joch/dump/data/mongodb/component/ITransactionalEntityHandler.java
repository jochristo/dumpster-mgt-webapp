package org.joch.dump.data.mongodb.component;

import java.util.Collection;

/**
 *
 * @author ic
 * @param <T>
 */
public interface ITransactionalEntityHandler<T extends Object>
{        
    public T save (T source, Collection<Object> included);
    
    
}
