package org.joch.dump.api.service;

import java.util.List;
import java.util.Map;
import org.joch.dump.api.domain.Dumpster;
import org.joch.dump.data.mongodb.predicate.BaseQueryPredicateService;
import org.joch.dump.api.repository.IBaseMongoRepository;
import org.springframework.stereotype.Component;

/**
 *
 * @author Admin
 */
@Component
public class DumpsterBaseService extends BaseQueryPredicateService<Dumpster>
{       

    
    @Override
    public List<Dumpster> findWithMap(Map<Object, Object> requestMap) {
        return super.findWithMap(requestMap); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Dumpster> findWithMap(IBaseMongoRepository<Dumpster> repository, Map<Object, Object> requestMap) {
        return super.findWithMap(repository, requestMap); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
