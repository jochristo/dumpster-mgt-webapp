package org.joch.dump.api.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import org.joch.dump.api.error.ApiErrorCode;
import org.joch.dump.api.exception.ResourceIdentifierNotFoundException;
import org.joch.dump.api.domain.Dumpster;
import org.joch.dump.data.mongodb.predicate.EntityPredicateBuilder;
import org.joch.dump.data.mongodb.predicate.PredicateOperation;
import org.joch.dump.api.repository.IDumpsterRepository;
import org.joch.dump.api.service.IDumpsterService;
import org.joch.dump.data.redis.service.IRedisHashService;
import org.joko.core.utilities.Utilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ic
 */
@Service
public class DumpsterServiceImpl implements IDumpsterService
{
    @Autowired
    private IDumpsterRepository repository;       
   
    @Autowired
    private IRedisHashService hashService;
    
    @Override
    public List<Dumpster> get() {
        return repository.findAll();
    }

    @Override
    public Dumpster findOne(String id) {
        return repository.findOne(id);
    }

    @Override
    public Dumpster save(Dumpster object) {        
        Dumpster dumpster =  repository.save(object);
        hashService.hAdd(Dumpster.class.getSimpleName(), dumpster.getId(), object);
        return dumpster;
    }

    @Override
    public Dumpster update(Object id, Dumpster object) {
        Dumpster dumpsterInfo = this.findOne("id", id);
        if(dumpsterInfo == null) throw new ResourceIdentifierNotFoundException(ApiErrorCode.RESOURCE_NOT_FOUND, "Dumpster id not found: " + id);        
        Dumpster item = dumpsterInfo.updateInstance(object);                               
        hashService.hAdd(Dumpster.class.getSimpleName(), id, item);        
        return repository.save(item);
    }

    @Override
    public void delete(Dumpster object) {
        repository.delete(object);
    }

    @Override
    public void insert(Dumpster object) {
        repository.insert(object);
    }

    @Override
    public Dumpster findOne(Object object) {        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public Dumpster findOne(String key, Object value) {
        EntityPredicateBuilder<Dumpster> predicate = new EntityPredicateBuilder(Dumpster.class);
        predicate.with(key, PredicateOperation.EQUALS, value);        
        Dumpster rvalue = (Dumpster) (Dumpster) hashService.hGet(Dumpster.class.getSimpleName(), value);
        return rvalue == null ? repository.findOne(predicate.build()) : rvalue;        
    }    

    @Override
    public List<Dumpster> get(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Dumpster> find(Map<Object, Object> requestMap) {
        if(Utilities.isEmpty(requestMap) == false)
        {            
            // querydsl methods            
            EntityPredicateBuilder<Dumpster> predicate = new EntityPredicateBuilder(Dumpster.class);              
            requestMap.forEach((new BiConsumer<Object, Object>() {
                @Override
                public void accept(Object key, Object value) {
                    if(!Utilities.isEmpty(key)){                                
                        predicate.with((String) key, PredicateOperation.EQUALS, value);
                    }
                }
            }));     
            List<Dumpster> list = new ArrayList();
            Iterable<Dumpster> iterable = repository.findAll(predicate.build());
            iterable.iterator().forEachRemaining(list::add);                        
            return list;            
        }
        return null;       
    
    }

    @Override
    public List<Dumpster> find(Map<Object, Object> requestMap, PredicateOperation operation) {
        
        if(Utilities.isEmpty(requestMap) == false)
        {            
            // querydsl methods            
            EntityPredicateBuilder<Dumpster> predicate = new EntityPredicateBuilder(Dumpster.class);              
            requestMap.forEach(((key, value) ->{            
                if(!Utilities.isEmpty(key)){
                    //predicate.with((String) key, ":", value);
                    Object[] readValue = EntityPredicateBuilder.determineValue(key, value);
                    //value = readValue;
                    predicate.with((String) key, operation, value, readValue);
                }                                
            }));     
            List<Dumpster> list = new ArrayList();
            Iterable<Dumpster> iterable = repository.findAll(predicate.build());
            iterable.iterator().forEachRemaining(list::add);                        
            return list;            
        }
        return null;         
        
    }


    
}
