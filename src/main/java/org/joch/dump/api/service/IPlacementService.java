package org.joch.dump.api.service;

import java.util.Date;
import java.util.List;
import org.joch.dump.api.domain.Placement;
import org.joch.dump.api.domain.enumeration.PlacementStatus;
import org.joch.dump.api.model.bind.PlacementQueryParameter;
import org.joch.dump.data.mongodb.service.IMongoDbService;

/**
 *
 * @author Admin
 */
public interface IPlacementService extends IMongoDbService<Placement>
{        
    public List<Placement> find(PlacementStatus status); 
        
    public List<Placement> find(double lonMin, double lonMax, double latMin, double latMax);    
        
    public List<Placement> findByDateIn(Date dateInFrom, Date dateInTo);     
        
    public List<Placement> findByDateOut(Date dateOut);
    
    public boolean exists(String id);       

    public boolean existsDumpsterReferenceId(String dumpsterReferenceId);              
    
    public List<Placement> findUndeleted();              
    
    public List<Placement> findDeleted();      
    
    public List<Placement> find(PlacementQueryParameter params);
    
    public boolean hasReservationDependency(String placementId);
}
