package org.joch.dump.api.service;

import java.util.List;
import org.joch.dump.api.domain.Customer;
import org.joch.dump.data.mongodb.service.IMongoDbService;

/**
 *
 * @author Admin
 */
public interface ICustomerService extends IMongoDbService<Customer>
{    
    public Customer findByEmail(String email);    
    
    public Customer findByMobileNo(String mobileNo);    
    
    public Customer findByPhoneNo(String phoneNo);    
    
    public List<Customer> findByLastname(String lastname);            
    
    public List<Customer> findUnDeleted();     
    
    public List<Customer> findDeleted();     
}
