package org.joch.dump.api.service;

import org.joch.dump.api.domain.Dumpster;
import org.joch.dump.data.mongodb.service.IMongoDbService;

/**
 *
 * @author Admin
 */
public interface IDumpsterService extends IMongoDbService<Dumpster>
{
    
}
