package org.joch.dump.api.service;

import java.util.Date;
import java.util.List;
import org.joch.dump.api.domain.Reservation;
import org.joch.dump.data.mongodb.service.IMongoDbService;

/**
 *
 * @author Admin
 */
public interface IReservationService extends IMongoDbService<Reservation>
{             
    public List<Reservation> findByDate(Date createdAt);         
    
    public boolean exists(String id);        
    
    public boolean existsCustomerId(String customerId);              
    
    public boolean existsPlacementId(String placementId);              
    
    public List<Reservation> findUndeleted();              
    
    public List<Reservation> findDeleted();        
    
}
