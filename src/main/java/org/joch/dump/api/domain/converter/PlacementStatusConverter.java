package org.joch.dump.api.domain.converter;

import org.joch.dump.api.domain.enumeration.PlacementStatus;
import org.springframework.core.convert.converter.Converter;

/**
 *
 * @author ic
 */
public class PlacementStatusConverter implements Converter<PlacementStatus, String>
{

    @Override
    public String convert(PlacementStatus s) {
        return s.getType();
    }
    
}
