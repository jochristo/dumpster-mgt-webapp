package org.joch.dump.api.repository;

import org.joch.dump.api.domain.Dumpster;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ic
 */
@Repository
public interface IDumpsterBaseRepository extends MongoRepository<Dumpster, String>, QueryDslPredicateExecutor<Dumpster>
{
    
}
