package org.joch.dump.api.repository;

import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Admin
 * @param <T>
 */
@Repository
public interface IBaseMongoRepository<T extends Object> extends QueryDslPredicateExecutor<T>
{

}
