package org.joch.dump.api.exception;

import org.joch.dump.api.error.ApiError;
import org.joch.dump.api.error.ApiErrorCode;

/**
 *
 * @author ic
 */
public class ResourceNotFoundException extends RestApplicationException
{
    public ResourceNotFoundException(String details) {
        super(details);
        super.setApiErrorCode(ApiErrorCode.RESOURCE_NOT_FOUND);
    }

    public ResourceNotFoundException(ApiError apiError, String details) {
        super(apiError, details);        
    }

    public ResourceNotFoundException(ApiErrorCode apiErrorCode, String details) {
        super(apiErrorCode, details);
        
    }
    
    
}
