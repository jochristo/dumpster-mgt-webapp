package org.joch.dump.api.exception;

import org.joch.dump.api.error.ApiError;
import org.joch.dump.api.error.ApiErrorCode;


/**
 *
 * @author ic
 */
public class ResourceIdentifierNotFoundException extends RestApplicationException
{
    public ResourceIdentifierNotFoundException(String details) {
        super(details);
        super.setApiErrorCode(ApiErrorCode.RESOURCE_NOT_FOUND);
    }

    public ResourceIdentifierNotFoundException(ApiError apiError, String details) {
        super(apiError, details);        
    }

    public ResourceIdentifierNotFoundException(ApiErrorCode apiErrorCode, String details) {
        super(apiErrorCode, details);
        
    }
    
    
}
