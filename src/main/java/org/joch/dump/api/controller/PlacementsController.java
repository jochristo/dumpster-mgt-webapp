package org.joch.dump.api.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import static org.joch.dump.api.Endpoints.PLACEMENTS;
import static org.joch.dump.api.Endpoints.PLACEMENT_BY_ID;
import org.joch.dump.api.Constants;
import org.joch.dump.api.error.ApiErrorCode;
import org.joch.dump.api.exception.ResourceIdentifierNotFoundException;
import org.joch.dump.api.exception.ResourceNotFoundException;
import org.joch.dump.api.domain.Placement;
import org.joch.dump.api.handler.IFieldValidationHandler;
import org.joch.dump.api.model.bind.PlacementQueryParameter;
import org.joch.dump.api.model.bind.converter.DatePropertyBindingConverter;
import org.joch.dump.api.service.IPlacementService;
import org.joch.dump.data.mongodb.service.IMorphiaService;
import org.joko.core.jsonapi.JSONApiData;
import org.joko.core.jsonapi.JSONApiDocument;
import org.joko.core.utilities.Utilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ic
 */
@RestController
public class PlacementsController
{
    private static final Logger logger = LoggerFactory.getLogger(PlacementsController.class);      

    @Autowired private IPlacementService placementService;        
    @Autowired private IFieldValidationHandler fieldValidationHandler;
    @Autowired private IMorphiaService morphiaService;    
  
    @InitBinder
    protected void initBinder(final WebDataBinder webdataBinder) {
        //webdataBinder.registerCustomEditor(MobilePlatform.class, new MobilePlatformPropertyBindingConverter());
        webdataBinder.registerCustomEditor(Date.class, new DatePropertyBindingConverter());
    } 
    
    @RequestMapping(value = PLACEMENTS, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.POST)
    @ResponseBody
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public JSONApiDocument createPlacement(@RequestBody JSONApiDocument document)
    {          
        JSONApiData data = (JSONApiData) document.getData();
        Placement placement = (Placement) data.convertTo(Placement.class);                        
        logger.info("Saving placement document...");                        
        placementService.insert(placement.newInstance());
        logger.info("Done.");
        logger.info("Loading document...");
        Placement pl = placementService.findOne(placement.getId());   
        if(pl == null){
            throw new ResourceIdentifierNotFoundException(ApiErrorCode.RESOURCE_NOT_FOUND, "Placement id not found: " + placement.getId());
        }        
        return new JSONApiDocument(pl);
    }          
    
    @RequestMapping(value = PLACEMENT_BY_ID, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET)  
    @ResponseBody
    public JSONApiDocument getPlacement(@PathVariable @NotNull String id)
    {         
        logger.info("Querying placement id..." + id);                        
        Placement pl = placementService.findOne(id);        
        if(pl == null){
            throw new ResourceIdentifierNotFoundException(ApiErrorCode.RESOURCE_NOT_FOUND, "Placement id not found: " + id);
        }
        logger.info("Document found");
        return new JSONApiDocument(pl);
    }         
    
    @RequestMapping(value = PLACEMENT_BY_ID, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.PUT)  
    @ResponseBody
    public JSONApiDocument updatePlacement(@PathVariable @NotNull String id, @RequestBody JSONApiDocument document)
    {                
        logger.info("Querying placement id..." + id);                                             
        Placement pl = placementService.findOne(id);            
        if(pl == null){
            throw new ResourceIdentifierNotFoundException(ApiErrorCode.RESOURCE_NOT_FOUND, "Placement id not found: " + id);
        }         
        logger.info("Document found");          
        
        JSONApiData data = (JSONApiData) document.getData();
        Placement placement = (Placement) data.convertTo(Placement.class);                      
        placementService.update(id,pl.updateInstance(placement));
        logger.info("Document updated");
        return new JSONApiDocument(pl);
    }      
    
    @RequestMapping(value = PLACEMENT_BY_ID, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.DELETE)  
    @ResponseBody
    public JSONApiDocument deletePlacement(@PathVariable @NotNull String id)
    {         
        logger.info("Querying placement id..." + id);                        
        Placement pl = placementService.findOne(id);        
        if(pl == null){
            throw new ResourceIdentifierNotFoundException(ApiErrorCode.RESOURCE_NOT_FOUND, "Placement id not found: " + id);
        }        
        logger.info("Document found");
        //placementService.delete(pl);               
        pl.setDeleted(true);
        placementService.update(id,pl);
        logger.info("Document deleted");
        return new JSONApiDocument(pl);
    }         
       
    //@RequestMapping(value = PLACEMENTS, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET)  
    //@ResponseBody
    public JSONApiDocument getPlacements(
        @RequestParam(value = "status", required = false) String status,
        @RequestParam(value = "dateIn", required = false) @DateTimeFormat(pattern = "dd/mm/yyyy") Date dateIn,
        @RequestParam(value = "longitude", required = false) String longitude,
        @RequestParam(value = "latitude", required = false) String latitude,
        @RequestParam(value = "createdAt", required = false) @DateTimeFormat(pattern = "dd/mm/yyyy") Date createdAt,
        @RequestParam(value = "operation", required = false) String operation
    )
    {
        Map<Object,Object> requestMap = new HashMap<>();
        if(!Utilities.isEmpty(status)){
            requestMap.put("status", status);        
            logger.info("Requested status: " + status);
        }
        if(!Utilities.isEmpty(dateIn)){
            requestMap.put("dateIn", dateIn);        
            logger.info("Requested dateIn: " + dateIn);
        }
        if(!Utilities.isEmpty(longitude)){
            requestMap.put("longitude", longitude);        
            logger.info("Requested longitude: " + longitude);
        }
        if(!Utilities.isEmpty(latitude)){
            requestMap.put("latitude", latitude);        
            logger.info("Requested latitude: " + latitude);
        }
        if(!Utilities.isEmpty(createdAt)){
            requestMap.put("createdAt", createdAt);        
            logger.info("Requested createdAt: " + createdAt);
        }        
        // find with multiple arguments
        if(requestMap.size() > 0)
        {
            logger.info("Querying MongoDB to find placement(s) with multiple arguments...");
            return new JSONApiDocument(placementService.find(requestMap));
        }
        
        logger.info("Querying MongoDB to find all placement information...");                        
        List<Placement> pl = placementService.get();
        if(pl == null){
            throw new ResourceNotFoundException(ApiErrorCode.RESOURCE_NOT_FOUND, "Placements not found");
        }
        logger.info("Document(s) found");
        return new JSONApiDocument(pl);
    }     
    
    @RequestMapping(value = PLACEMENTS, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET)  
    @ResponseBody
    public JSONApiDocument getPlacementsModel(@ModelAttribute @Valid PlacementQueryParameter parameter, BindingResult result)
    {
        // field validation
        if(result.hasErrors()){            
            fieldValidationHandler.resolveErrors(result.getFieldErrors());                        
        }               
        
        logger.info("Running placement query...");                        
        List<Placement> pl = null;    
        if(parameter == null){
            pl = placementService.get();             
        }
        else
        {
           pl = placementService.find(parameter);
        }
        logger.info( pl.size() + " document(s) found");
        return new JSONApiDocument(pl);
    }
    
    
}
