package org.joch.dump.api.model;

import org.joch.dump.api.annotation.DateAttribute;
import org.joch.dump.api.domain.enumeration.PlacementStatus;

/**
 *
 * @author ic
 */
public class PlacementQuery
{    
    @DateAttribute
    private String dateIn;
    
    @DateAttribute
    private String createdAt;
    
    private double longitude;
    
    private double latitude;
    
    private PlacementStatus status;
    
}
