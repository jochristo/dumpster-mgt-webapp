package org.joch.dump.api.model;

import org.joch.dump.api.annotation.DateAttribute;

/**
 *
 * @author Admin
 */
public class ReservationQuery
{    
    @DateAttribute
    private String createdAt;
    
    private CustomerQuery customerQuery;

    public ReservationQuery(String createdAt, CustomerQuery customerQuery) {
        this.createdAt = createdAt;
        this.customerQuery = customerQuery;
    }
        
    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public CustomerQuery getCustomerQuery() {
        return customerQuery;
    }

    public void setCustomerQuery(CustomerQuery customerQuery) {
        this.customerQuery = customerQuery;
    }
    
    
}
