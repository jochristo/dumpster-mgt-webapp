package org.joch.dump.api.configuration;

import org.joch.dump.api.interceptor.LogInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 *
 * @author ic
 */
@Configuration
public class WebMvcConfiguration extends WebMvcConfigurerAdapter
{    
    @Autowired
    LogInterceptor loggingInterceptor;    

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loggingInterceptor); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
